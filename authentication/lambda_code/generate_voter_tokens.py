import boto3
from secrets import token_urlsafe


def lambda_handler(event, context):
    dynamodb = boto3.client('dynamodb', region_name='eu-west-2')
    voter_ids = event['voter_ids']
    voter_id_chunks = chunks(voter_ids, 25) # batch_write_item can do 25 puts at a time
    responses = []
    for voter_id_chunk in voter_id_chunks:
        response = dynamodb.batch_write_item(
            RequestItems={
                'VoterAuthenticationTokens': [
                    generate_put_request(voter_id) for voter_id in voter_id_chunk
                ]
            }
        )
        responses.append(response)

    return {
        'statusCode': 200,
        'responses': responses
    }

def chunks(voter_ids, n):
    """Yield successive n-sized chunks from voter_ids."""
    for i in range(0, len(voter_ids), n):
        yield voter_ids[i:i + n]

def generate_put_request(voter_id):
    """Generate a put request object with a security token"""
    put_request = {
        'PutRequest':{
            'Item': {
                'VoterId': { 'S': str(voter_id) },
                'VoterToken': { 'S': token_urlsafe(32) }
            }
        }
    }

    return put_request;
