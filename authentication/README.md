# Purpose
This directory contains infrastructure definitions and code packages for generating auth tokens

# Running unit tests
To run the unit tests, build an image
```
docker build -t test .
```

And then run the tests
```
docker run -it --rm test -m unittest <path-to-test-file>
```

All in a single command:
```
docker build -t test . && docker run -it --rm test -m unittest <path-to-test-file>
```
