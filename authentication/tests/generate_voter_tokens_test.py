import unittest
from unittest.mock import patch
import boto3
import botocore.session
from botocore.stub import Stubber
from lambda_code import generate_voter_tokens

class TestGenerateVoterTokens(unittest.TestCase):

    @patch.object(boto3, 'client')
    def test_response_code(self, mock_client):
        response = {
            "UnprocessedItems": {},
            "ResponseMetadata": {
                "RequestId": "4LT36BR4SUJT8QNH7RLC8N8ACJVV4KQNSO5AEMVJF66Q9ASUAAJG",
                "HTTPStatusCode": 200,
                "HTTPHeaders": {
                    "server": "Server",
                    "date": "Sun, 28 Jun 2020 15:24:38 GMT",
                    "content-type": "application/x-amz-json-1.0",
                    "content-length": "23",
                    "connection": "keep-alive",
                    "x-amzn-requestid": "4LT36BR4SUJT8QNH7RLC8N8ACJVV4KQNSO5AEMVJF66Q9ASUAAJG",
                    "x-amz-crc32": "4185382651"
                },
                "RetryAttempts": 0
            }
        }
        stubber = TestGenerateVoterTokens.mock_dynamodb(mock_client, response)
        with stubber:
            result = generate_voter_tokens.lambda_handler({ 'voter_ids': [1, 2, 3, 4] }, None)
            self.assertEqual(result['statusCode'], 200)

    @staticmethod
    def mock_dynamodb(mock_client, response):
        dynamodb = botocore.session.get_session().create_client('dynamodb', region_name='eu-west-2')
        stubber = Stubber(dynamodb)
        stubber.add_response('batch_write_item', response)
        mock_client.return_value = dynamodb
        return stubber

if __name__ == '__main__':
    unittest.main()
